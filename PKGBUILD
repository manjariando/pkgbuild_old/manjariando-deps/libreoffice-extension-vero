# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Davi da Silva Böger <dsboger@gmail.com>
# Contributor: Anizio de Oliveira Rodrigues <aniziooliveira@bol.com.br>

pkgname=libreoffice-extension-vero
_pkgname=VeroptBRV320AOC
pkgver=3.2
pkgrel=5
pkgdesc="A spell checker and grammar checker for LibreOffice and OpenOffice.org - Brazilian Portuguese."
arch=('any')
url="https://pt-br.libreoffice.org/projetos/vero"
license=('LGPL3' 'MPL')
groups=('libreoffice-extensions')
source=("https://pt-br.libreoffice.org/assets/Uploads/PT-BR-Documents/VERO/${_pkgname}.oxt"
        "https://metainfo.manjariando.com.br/${pkgname}/org.vero.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/vero"-{48,64,128}.png)
md5sums=('9a9ac174c401406604bfcf101de2a7ba'
         '2fe288197bbe7c51d0b49ff71094fa2c'
         '471c933b9b1af210a53468f7a0d45cec'
         'cb4f9ae281af491b99354cbc86be3965'
         '33a3b30864666e74215cb780212a591b')
noextract=("${_pkgname}.oxt")

prepare() {
    mkdir -p "${srcdir}/${_pkgname}"
    cd "${srcdir}/${_pkgname}"
    bsdtar -xf ${srcdir}/${_pkgname}.oxt
}

_vero_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Office;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_vero_desktop" | tee org.vero.desktop
}

package () {
    depends=('libreoffice')

    install -d "${pkgdir}/usr/lib/libreoffice/share/extensions/"
    cp -R "${srcdir}/${_pkgname}" "${pkgdir}/usr/lib/libreoffice/share/extensions/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/org.vero.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.vero.metainfo.xml"
    install -Dm644 "${srcdir}/org.vero.desktop" \
      "${pkgdir}/usr/share/applications/org.vero.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/vero-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/vero.png"
    done
}

